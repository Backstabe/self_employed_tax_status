# frozen_string_literal: true

require 'httparty'

require 'self_employed_tax_status/version'
require 'self_employed_tax_status/client'
require 'self_employed_tax_status/validate_params'
require 'self_employed_tax_status/result_helper'

# SelfEmployedTaxStatus module
module SelfEmployedTaxStatus
  class Error < StandardError; end

  extend ResultHelper

  class << self
    def check(inn:, date:)
      status = validation(inn, date)
      return status if !status.nil? && status.error?

      res = Client.new.taxpayer_status(inn, date)

      return result_422('validation.failed.wrong.inn_number') if vrong_inn?(res)

      prepare_ok_result(res)
    end

    private

    def validation(inn, date)
      return result_422('validation.failed.wrong.inn_format') unless
        ValidateParams.correct_inn?(inn)

      return result_422('validation.failed.wrong.date.format') unless
        ValidateParams.correct_date_format?(date)

      return result_422('validation.failed.wrong.date.period') unless
        ValidateParams.date_in_period?(date)
    end

    def vrong_inn?(res)
      res.code == 422 &&
        res.parsed_response['code'] == 'validation.failed'
    end

    def prepare_ok_result(res)
      return unless res.code == 200

      if res.parsed_response['status']
        result_ok('self_employed')
      else
        result_ok('not_self_employed')
      end
    end
  end
end
