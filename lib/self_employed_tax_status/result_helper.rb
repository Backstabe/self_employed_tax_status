# frozen_string_literal: true

# Results struct format
module ResultHelper
  Result = Struct.new(:result, :status, :code) do
    def ok?
      result == :ok
    end

    def error?
      result == :error
    end

    def self_employed?
      return nil if !result == :ok

      case code
      when 'self_employed'
        true
      when 'not_self_employed'
        false
      end
    end
  end

  def result_struct(result, status, code)
    Result.new(result, status, code)
  end

  def result_422(code)
    result_struct(:error, 422, code)
  end

  def result_ok(code)
    result_struct(:ok, 200, code)
  end

  def result_bad_request(code)
    result_struct(:error, 400, code)
  end
end
