# frozen_string_literal: true

module SelfEmployedTaxStatus
  # prepare data to request
  class ValidateParams
    class << self
      def correct_date_format?(date)
        valid_date?(date)
      end

      def correct_inn?(inn)
        #  TODO check digit calculation
        # https://clck.ru/9cDtq
        #
        # !! to transform true/false
        !!(inn =~ /^\d{12}$/)
      end

      def date_in_period?(date)
        Date.new(2019, 1, 1) < parse_date(date) &&
          parse_date(date) <= Date.today
      end

      private

      def valid_date?(str)
        parse_date(str)
      rescue
        false
      end

      def parse_date(str)
        Date.strptime(str, '%Y-%m-%d')
      end
    end
  end
end
