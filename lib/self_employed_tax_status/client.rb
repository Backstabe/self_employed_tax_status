# frozen_string_literal: true

module SelfEmployedTaxStatus
  # HTTP client
  class Client
    include HTTParty

    base_uri 'https://statusnpd.nalog.ru:443/api'

    def initialize
      @headers = {
        'Content-Type' => 'application/json'
      }
    end

    def taxpayer_status(inn, date)
      self.class.post(
        '/v1/tracker/taxpayer_status',
        headers: @headers,
        body: {
          inn: inn,
          requestDate: date
        }.to_json
      )
    end
  end
end
