lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'self_employed_tax_status/version'

Gem::Specification.new do |spec|
  spec.name          = 'self_employed_tax_status'
  spec.version       = SelfEmployedTaxStatus::VERSION
  spec.authors       = ['Polyakov Alexandr']
  spec.email         = ['backstabe@gmail.com']

  spec.summary       = %q{Self employed tax status}
  spec.description   = %q{Check self employed tax status by INN}
  spec.homepage      = 'https://bitbucket.org/godsofms/self_employed_tax_status/src/master/'
  spec.licenses      = ['MIT']

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end

  spec.require_paths = ['lib']

  spec.add_dependency 'httparty', '~> 0.15.6'
  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
end
