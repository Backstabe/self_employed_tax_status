# frozen_string_literal: true

RSpec.describe SelfEmployedTaxStatus do
  it 'has a version number' do
    expect(SelfEmployedTaxStatus::VERSION).not_to be nil
  end

  describe '.check' do
    subject { described_class.check(params) }

    context 'wrong data format' do
      let(:params) { { inn: '123412341234', date: 'adf' } }

      it 'return code' do
        expect(subject.code).to eq('validation.failed.wrong.date.format')
      end

      it 'return error result' do
        expect(subject.error?).to be_truthy
      end

      it 'status 422' do
        expect(subject.status).to eq(422)
      end
    end

    context 'wrong inn format' do
      let(:params) { { inn: 'd23412341234', date: '2019-01-30' } }

      it 'return code' do
        expect(subject.code).to eq('validation.failed.wrong.inn_format')
      end

      it 'return error result' do
        expect(subject.error?).to be_truthy
      end

      it 'status 422' do
        expect(subject.status).to eq(422)
      end
    end

    context 'date not in period' do
      let(:params) { { inn: '123412341234', date: '2018-01-30' } }

      it 'return code' do
        expect(subject.code).to eq('validation.failed.wrong.date.period')
      end

      it 'return error result' do
        expect(subject.error?).to be_truthy
      end

      it 'status 422' do
        expect(subject.status).to eq(422)
      end
    end

    context 'self employed', :vcr do
      let(:params) { { inn: '550114632986', date: '2019-06-27' } }

      it 'return code' do
        expect(subject.code).to eq('self_employed')
      end

      it '.self_employed?' do
        expect(subject.self_employed?).to be_truthy
      end

      it 'status 200' do
        expect(subject.status).to eq(200)
      end
    end

    context 'not self employed', :vcr do
      let(:params) { { inn: '524809691404', date: '2019-06-27' } }

      it '.self_employed?' do
        expect(subject.self_employed?).to be_falsey
      end

      it 'return code' do
        expect(subject.code).to eq('not_self_employed')
      end

      it 'status 200' do
        expect(subject.status).to eq(200)
      end
    end

    context 'wrong inn', :vcr do
      let(:params) { { inn: '524809691405', date: '2019-06-27' } }

      it 'return code' do
        expect(subject.code).to eq('validation.failed.wrong.inn_number')
      end

      it 'return error result' do
        expect(subject.error?).to be_truthy
      end

      it 'status 422' do
        expect(subject.status).to eq(422)
      end
    end
  end
end
