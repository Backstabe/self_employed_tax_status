# frozen_string_literal: true

RSpec.describe SelfEmployedTaxStatus::ValidateParams do
  subject { described_class }

  describe '.correct_inn?' do
    it 'correct inn return true' do
      expect(subject.correct_inn?('123412341234')).to be_truthy
    end

    it 'short number return false' do
      expect(subject.correct_inn?('12341234123')).to be_falsey
    end

    it 'long number return false' do
      expect(subject.correct_inn?('1234123412341')).to be_falsey
    end

    it 'with letters return false' do
      expect(subject.correct_inn?('1a3412341234')).to be_falsey
    end

    it 'with spaces return false' do
      expect(subject.correct_inn?('1 341234123411')).to be_falsey
    end
  end

  describe '.correct_date_format?' do
    it 'correct date return true' do
      expect(subject.correct_date_format?('2019-01-30')).to be_truthy
    end

    it 'incorrect date return false' do
      expect(subject.correct_date_format?('2019.01.30')).to be_falsey
    end
  end

  describe '.date_in_period?' do
    it 'data < 2019-01-01 return false' do
      expect(subject.date_in_period?('2018-01-30')).to be_falsey
    end

    it 'date > now return false' do
      expect(
        subject.date_in_period?(Date.today.next_day(1).iso8601)
      ).to be_falsey
    end

    it 'data > 2019-01-01 return false' do
      expect(subject.date_in_period?('2019-01-30')).to be_truthy
    end
  end
end
