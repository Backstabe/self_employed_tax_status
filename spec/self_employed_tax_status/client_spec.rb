# frozen_string_literal: true

RSpec.describe SelfEmployedTaxStatus::Client do
  let!(:inn) { '550114632986' }
  let!(:inn_wrong) { '524809691404' }

  describe '#taxpayer_status', :vcr do
    subject { described_class.new.taxpayer_status(*params) }

    context 'self employe' do
      let(:params) { [inn, '2019-06-27'] }

      it 'status true' do
        expect(subject.parsed_response['status']).to be_truthy
      end

      it 'status code 200' do
        expect(subject.code).to eq(200)
      end
    end

    context 'bad request' do
      let(:params) { %w[some some] }

      it 'return string' do
        result = {
          message: 'Неверный формат запроса',
          operationId: nil
        }

        expect(subject.parsed_response.to_json).to eq(result.to_json)
      end

      it 'status code 400' do
        expect(subject.code).to eq(400)
      end
    end

    context 'not self employed' do
      let(:params) { [inn_wrong, '2019-07-04'] }

      it 'status false' do
        expect(subject.parsed_response['status']).to be_falsey
      end

      it 'message include keys' do
        expect(subject.parsed_response.keys).to include('status', 'message')
      end

      it 'status code 200' do
        expect(subject.code).to eq(200)
      end
    end

    context 'not self employed 10 days bafore' do
      let(:params) { [inn_wrong, Date.today.prev_day(10).iso8601] }

      it 'status false' do
        expect(subject.parsed_response['status']).to be_falsey
      end

      it 'status code 200' do
        expect(subject.code).to eq(200)
      end
    end

    context 'date too early' do
      let(:params) { [inn_wrong, '2018-01-01'] }

      it 'return validation failed' do
        expect(subject.parsed_response['code']).to eq('validation.failed')
      end

      it 'status code 422' do
        expect(subject.code).to eq(422)
      end
    end

    context 'data greater than today' do
      let(:params) { [inn_wrong, Date.today.next_day(10).iso8601] }

      it 'return validation failed' do
        expect(subject.parsed_response['code']).to eq('validation.failed')
      end

      it 'status code 422' do
        expect(subject.code).to eq(422)
      end
    end

    context 'to many requests' do
      let(:params) { [inn, '2019-06-27'] }

      it 'faled code' do
        expect(subject.parsed_response['code'])
          .to eq('taxpayer.status.service.limited.error')
      end

      it 'status code 422' do
        expect(subject.code).to eq(422)
      end
    end
  end
end
